#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 14:41:23 2023

@author: zahrouni
"""


print('[i] Training Process is starting  ... ')


import matplotlib.pyplot as plt
import numpy as np
import os
import random
import tensorflow as tf
import SN_architecture as sna 

from glob import glob
from keras.callbacks import EarlyStopping
from tensorflow.keras import optimizers

#function to preprocess : convert and resize imgs 
def preprocess(filename):
    try:
        img_string = tf.io.read_file(filename)
        img= tf.image.decode_jpeg(img_string, channels=3)
        img= tf.image.convert_image_dtype(img, tf.float32)
        img= tf.image.resize(img, target_shape)
        return img
    except Exception as e:
        print(f'Error PreProcessing file {filename}: {e}')
        return None 

#function to preprocess triplet 

def preprocess_triplet(anc, pos, neg):
    return (
        preprocess(anc),
        preprocess(pos),
        preprocess(neg),
    )

#function to visualise triplet 
def visualize(anc, pos, neg):
    def show(ax, img, title):
        ax.imshow(img)
        ax.set_title(title)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    fig = plt.figure(figsize=(9,9))
    axs = fig.subplots(3,3)
    
    for i in range(3):
        show(axs[i, 0], anc[i],"Anchor")
        show(axs[i, 1], pos[i],"Positive")
        show(axs[i, 2], neg[i], "Negative")
    
    fig.tight_layout()
    plt.savefig(f'{Tref}/triplet_samples.png')
    plt.close(fig)
         



#define hyperparameter 

batch_size = 128 #BatchSize 
epochs = 25 #Number of Training Epochs 
init_lr_rate = 2e-6 #The learning Rate 
target_shape = (224, 224) #The shape of the input imgs 


Tref = f'M1_{batch_size}B_{epochs}E_lr{init_lr_rate}' #Training reference 

#create a training folder 
if not os.path.exists(Tref):
    os.makedirs(Tref)

#path to the training dataset 
data_path = '/path/to_training/dataset/'

print(f" [i] Trying to access file: {data_path}",flush=True)
print(f" [i] Current working directory: {os.getcwd()}", flush=True)
print(f' [i] Training ref = {Tref}')
print('[i] Start loading data ...')

#Loading data and creating a Triplet dataset for training and validation 

#paths lists for anchor positive and negative imgs 
anchor_images_path = []
positive_images_path = []
negative_imges_path = [] 

#getting all classes from the dataset directory 
all_classes = os.listdir(data_path)

#classifiy imgs of each class into anchor and postive classes 
for class_ in all_classes: 
    class_imgs_path = glob(f'{data_path}/{class_}/*.jpg')
    
    if ((len(class_imgs_path) % 2) and (len(class_imgs_path)!=1)):
        class_imgs_path = class_imgs_path[1:]
    n = len(class_imgs_path) // 2

    anchor_images_path.extend(class_imgs_path[:n]) # select some person as anchors
    positive_images_path.extend(class_imgs_path[n:]) # select some person as positive samples

#a dictionary to hold images of each class 
class_imgs = {cls: glob(f'{data_path}/{cls}/*.jpg') for cls in all_classes}

#selecting negative imgs different from the anchor class 
for anchor_img_path in anchor_images_path:
    
    anchor_class = os.path.basename(os.path.dirname(anchor_img_path))
    other_classes = [cls for cls in all_classes if cls != anchor_class and len(class_imgs[cls]) > 0]
    negative_class = random.choice(other_classes)
    
    #randomly select a negative img from the negative class, 
    #and remove it from the class_img 
    #to avoide using the same img twice or more 
    negative_img = random.choice(class_imgs[negative_class])
    class_imgs[negative_class].remove(negative_img)
    
    negative_imges_path.append(negative_img)
    
    
        
# make all lists of equal length to maintain balance 
min_len = min(
    len(anchor_images_path),
    len(positive_images_path),
    len(negative_imges_path),
)    
  
anchor_images_path = anchor_images_path[:min_len]
positive_images_path = positive_images_path[:min_len]  
negative_imges_path = negative_imges_path[:min_len]
#forming triplet 
anc_img = (anchor_images_path)
pos_img = (positive_images_path)
neg_img = (negative_imges_path)

triplets = list(zip(anc_img,pos_img,neg_img))
#shuffling 
rng = np.random.RandomState(seed=42)
rng.shuffle(triplets)
anc_img ,pos_img, neg_img = zip(*triplets)
data_size = len(anc_img)

#Creating the tensorflow dataset 
anc_dataset = tf.data.Dataset.from_tensor_slices(list(anc_img))
pos_dataset = tf.data.Dataset.from_tensor_slices(list(pos_img))
neg_dataset = tf.data.Dataset.from_tensor_slices(list(neg_img))

#zipping and shuffling the dataset 
dataset = tf.data.Dataset.zip((anc_dataset, pos_dataset, neg_dataset))
dataset = dataset.shuffle(buffer_size=1024)

print('[i] Dataset is ready ...')
print('[i] Dataset size = ', data_size)



#preprocess the triplet-dataset (the imgs)
dataset = dataset.map(preprocess_triplet)

print('[i] Split the dataset to Train & Validation subset',flush=True)
batch_size2 = int(batch_size/2)

# Split the dataset into training (90%) and validation (10%)
train_dataset = dataset.take(round(data_size * 0.9))
val_dataset = dataset.skip(round(data_size * 0.9))

# Batch the Training and the Validation Dataset to the specified batch size 
# Prefetch the batches for faster access 
train_dataset = train_dataset.batch(batch_size, drop_remainder=False)
train_dataset = train_dataset.prefetch(batch_size2)

val_dataset = val_dataset.batch(batch_size, drop_remainder=False)
val_dataset = val_dataset.prefetch(batch_size2)

#visualize a Triplet-Samples to check the dataset 
visualize(*list(train_dataset.take(1).as_numpy_iterator())[0])   

print('[i] Samples are visualized ...') 
print('==========================================================') 
print('[i] Building the Siamese Network ... ')


embedding = sna.get_embedding_model(target_shape)

siamese_network =sna.get_siamese_net(target_shape, embedding)

siamese_model = sna.SiameseModel(siamese_network)

print('==========================================================')

#define the Adam optimizer 
optimizer_ = optimizers.Adam(learning_rate=init_lr_rate)

print('[i] siamese model is ready to compile ...')
#compile the siamese networkmodel 
siamese_model.compile(optimizer=optimizer_)
print(f'Learning rate = {init_lr_rate}')

print('==========================================================')


#print the network summary 
siamese_network.summary() 
#define the early stopping callback , to stop the training, 
#when the validation loss is not improving. 
early_stopping = EarlyStopping(monitor='val_loss', patience=5)

print('==========================================================')

print('[i] Model is ready for Training ... ')

#start the model training 
history=siamese_model.fit(train_dataset, epochs=epochs, validation_data=(val_dataset), callbacks=[early_stopping])

#save the model weights 
siamese_model.save_weights(f'{Tref}/siamese_model_weights_{Tref}.h5')

print('[i] Model weights are saved')


print('==========================================================')


#plot the training and validation Loss 

plt.figure()
plt.plot(history.history['loss'], label ='Train Loss' )
plt.plot(history.history['val_loss'], label = 'validation Loss')
plt.title('Loss')
plt.xlabel('Number of Epochs')
plt.ylabel('Loss')
plt.legend(['TRAIN', 'VAL'], loc='upper right')
plt.savefig(f'{Tref}/Loss_plot_{Tref}.pdf') 
plt.close()


#plot the training and validation accuracy 

plt.figure()
plt.plot(history.history['accuracy'], label ='Train accuracy' )
plt.plot(history.history['val_accuracy'], label = 'validation accuracy')
plt.title('Accuracy')
plt.xlabel('Number of Epochs')
plt.ylabel('accuracy')
plt.legend(['TRAIN', 'VAL'], loc='lower right')
plt.savefig(f'{Tref}/Accuracy_plot_{Tref}.pdf') 
plt.close()


