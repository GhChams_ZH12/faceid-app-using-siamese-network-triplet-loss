#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 31 10:58:02 2023

@author: zahrouni
"""
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.applications import resnet50
from tensorflow.keras import layers
from tensorflow.keras.models import Model



#custom layer for computing distances in the Siamese Network 
class DistanceLayer(layers.Layer):
    """
    This layer computes the distance between:
        (the anchor embedding and the positive embedding), 
    and (the anchor embedding and the negative embedding).
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, anchor, positive, negative):
        ap_distance = tf.reduce_sum(tf.square(anchor - positive), -1)
        an_distance = tf.reduce_sum(tf.square(anchor - negative), -1)
        
        return (ap_distance, an_distance)



#Siamese Network model with custom training and testing loops 
class SiameseModel(Model):
    """The Siamese Network model with a custom training and testing loops.

    Computes the triplet loss using the three embeddings produced by the
    Siamese Network.

    The triplet loss is defined as:
       L(A, P, N) = max(‖f(A) - f(P)‖² - ‖f(A) - f(N)‖² + margin, 0)
    """

    def __init__(self, siamese_network, margin=0.5):
        super(SiameseModel, self).__init__()
        self.siamese_network = siamese_network
        self.margin = margin
        self.loss_tracker = keras.metrics.Mean(name="loss")
        self.accuracy_tracker = keras.metrics.Mean(name='accuracy')
        
        
    def call(self, inputs):
        return self.siamese_network(inputs)

    def train_step(self, data):
        # GradientTape is a context manager that records every operation that
        # you do inside. We are using it here to compute the loss so we can get
        # the gradients and apply them using the optimizer specified in
        # `compile()`.
        with tf.GradientTape() as tape:
            loss = self._compute_loss(data)

        # Storing the gradients of the loss function with respect to the weights/parameters.
        gradients = tape.gradient(loss, self.siamese_network.trainable_weights)

        # Applying the gradients on the model using the specified optimizer
        self.optimizer.apply_gradients(
            zip(gradients, self.siamese_network.trainable_weights)
        )

        # update and return the training loss metric.
        self.loss_tracker.update_state(loss)
        #update and return the accuracy
        accuracy = self._compute_accuracy(data)
        self.accuracy_tracker.update_state(accuracy)
        
        return {"loss": self.loss_tracker.result(), 
                "accuracy":self.accuracy_tracker.result()}

    def test_step(self, data):
        loss = self._compute_loss(data)
        # update and return the loss metric.
        self.loss_tracker.update_state(loss)
        # update and return the accuracy metric 
        accuracy = self._compute_accuracy(data)
        self.accuracy_tracker.update_state(accuracy)
        
        return {"loss": self.loss_tracker.result(), 
                "accuracy" : self.accuracy_tracker.result()}

    def _compute_loss(self, data):
        # The output of the network is a tuple containing the distances
        # between the anchor and the positive example, and the anchor and
        # the negative example.
        ap_distance, an_distance = self.siamese_network(data)

        # Computing the Triplet Loss by subtracting both distances and
        # making sure we don't get a negative value.
        loss = ap_distance - an_distance
        loss = tf.maximum(loss + self.margin, 0.0)
        return loss
    
    def _compute_accuracy(self,data): 
        #get the output of the network 
        ap_distance, an_distance = self.siamese_network(data)
        
        #computing the accuracy of the siamese network using the distances 
        #between the anchor and the positive example, and the anchor and 
        #the negative example: how often the anchor positive distance is less 
        #then the anchor negative distance 
        accuracy = tf.reduce_mean(tf.cast(ap_distance < an_distance, tf.float32))
        return accuracy 
    
    
    @property
    def metrics(self):
        # We need to list our metrics here so the `reset_states()` can be
        # called automatically.
        return [self.loss_tracker, self.accuracy_tracker]
    
        
    
    
    def get_config(self):
        # This method is necessary for serializing the model.
        base_config = super().get_config()
        config = {
            "siamese_network": tf.keras.utils.serialize_keras_object(self.siamese_network),
            "margin": tf.keras.saving.serialize_keras_object(self.margin),
            "loss_tracker": tf.keras.saving.serialize_keras_object(self.loss_tracker),
            "accuracy_tracker": tf.keras.saving.serialize_keras_object(self.accuracy_tracker),
        }
        return {**base_config, **config}
    
    
    
    @classmethod
    def from_config(cls, config):
        config['siamese_network'] = tf.keras.saving.deserialize_keras_object(config.pop('siamese_network'))
        config['margin'] = tf.keras.saving.deserialize_keras_object(config.pop('margin'))
        config['loss_tracker'] = tf.keras.saving.deserialize_keras_object(config.pop('loss_tracker'))
        config['accuracy_tracker'] = tf.keras.saving.deserialize_keras_object(config.pop('accuracy_tracker'))
        return cls(**config)
        



#M1 Model : resnet50 based model

#Function to create the embedding model using resnet50 

def get_embedding_model (shape):
    """     
    Creates an embedding model based on RESNET50 
    
    Parameters 
    ----------
    shape: tuple
        The shape of the input images       
        
    Returns
    -------
    embedding: Model
        A keras Model instance. 
     
    """
    input_shape = shape + (3,)
    
    base_cnn = resnet50.ResNet50(weights='imagenet', input_shape= input_shape, include_top=False) 

    x = layers.GlobalAveragePooling2D()(base_cnn.output)
    x = layers.Dense(1024, activation="relu")(x)
    x = layers.Dropout(0.5)(x)
    x = layers.BatchNormalization()(x)
    x = layers.Dense(512, activation="relu")(x)
    x = layers.Dropout(0.5)(x)
    x = layers.BatchNormalization()(x)
    x = layers.Dense(256, activation="relu")(x)
    x = layers.Dropout(0.5)(x)
    outputs = layers.Dense(128)(x)
    
    embedding = Model(base_cnn.input, outputs, name = 'embedding')
    
    #set non-trainable layers 
    trainable = False 
    for layer in base_cnn.layers:
        if layer.name == 'conv5_block1_out':
            trainable = True 
        layer.trainable = trainable
        
    return embedding 

#Function to create the Siamese Network Model using the embedding model 
def get_siamese_net(input_shape, embedding_model):
    """
    Creates a Siamese Network using the provided embedding model.

    Parameters
    ----------
    input_shape : tuple
        The shape of the input images.
    embedding_model : Model
        The embedding model to use.

    Returns
    -------
    siamese_network : Model
        A Keras Model instance representing the Siamese Network.

    """
    anc_input = layers.Input(name='anchor__' , shape = input_shape+ (3,))
    pos_input = layers.Input(name='positive' , shape = input_shape+(3,))
    neg_input = layers.Input(name='negative' , shape = input_shape+(3,))
    
    distances = DistanceLayer()(
    
    embedding_model(resnet50.preprocess_input(anc_input)),
    embedding_model(resnet50.preprocess_input(pos_input)),
    embedding_model(resnet50.preprocess_input(neg_input)),
    )
    
    siamese_network = Model(
        inputs=[anc_input, pos_input, neg_input],
        outputs=distances
    )
    return siamese_network


#M2 Model: customized model 

def get_custom_embedding_model(input_shape):
    """
    Creates a custom embedding model 

    Parameters
    ----------
    input_shape : tuple
        The shape of the input images.

    Returns
    -------
    Model
        A keras Model instance..

    """
    
    
    # Input layer
    input_layer = layers.Input(shape=(224, 224, 3))
    
    # First block
    x = layers.Conv2D(64, (3, 3), activation='relu')(input_layer)
    x = layers.MaxPooling2D((2, 2))(x)
    
    # Second block
    x = layers.Conv2D(32, (3, 3), activation='relu')(x)
    x = layers.MaxPooling2D((2, 2))(x)
    x = layers.Dropout(0.4)(x)

    
    # Fully connected layers
    x = layers.Flatten()(x)
    x = layers.Dense(128, activation='relu')(x)


    return Model(inputs=input_layer, outputs=x, name="custom_embedding")

def get_custom_siamese_net(input_shape, embedding_model):
    """
    Creates a custom Siamese Network using the provided customized embedding model.

    Parameters
    ----------
    input_shape : tuple
        The shape of the input images.
    embedding_model : Model
        The embedding model to use.


    Returns
    -------
    siamese_network : Model
        A Keras Model instance representing the costumized Siamese Network.

    """
    anc_input = layers.Input(name='anchor__' , shape = input_shape+ (3,))
    pos_input = layers.Input(name='positive' , shape = input_shape+(3,))
    neg_input = layers.Input(name='negative' , shape = input_shape+(3,))
    
    distances = DistanceLayer()(
    
    embedding_model(anc_input),
    embedding_model(pos_input),
    embedding_model(neg_input),
    )
    
    siamese_network = Model(
        inputs=[anc_input, pos_input, neg_input],
        outputs=distances
    )
    return siamese_network



































