# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 12:33:09 2023

@author: Zahro
"""


import sys
import struct 
import os 
import cv2
import socket 
import atexit


from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QVBoxLayout, QWidget, QTextEdit
from PyQt5.QtWidgets import QFileDialog, QMessageBox

from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import QTimer
from mtcnn import MTCNN

#MTCNN face detector 
detector = MTCNN()

#Server  IP 
SERVER_HOST = '192.178.xxx.xxx'
#Server Port 
SERVER_PORT = 9999 
#init client with none 
client_socket = None
#path to the captured img 
pic_path = 'path/to/captured/img'

class OutputWriter:
    """
    a utility class to write/display text outputs to a GUI widget. 
    """
    def __init__(self, output_widget):
        """
        Initializes the OutputWriter with a specific GUI widget.
        """
        self.output_widget = output_widget

    def write(self, text):
        """
        writes the given text to the output widget. 
        """
        cursor = self.output_widget.textCursor()
        cursor.movePosition(cursor.End)
        cursor.insertText(text)
        self.output_widget.setTextCursor(cursor)
        self.output_widget.ensureCursorVisible()

    def flush(self):
        #a place holder for compatibility, 
        #to clear internal buffers
        pass
    


class FaceId(QMainWindow):
    """ Main class for the FaceID_App using PyQt5 """
    
    def __init__(self):
        """ initialize the main window of the app."""
        super().__init__()
        self.setWindowTitle("FaceID")
        self.setGeometry(750, 400, 400, 300)
        self.initUI()
        atexit.register(self.cleanup)


    
    def face_detection(self,img):
        """
        A function to detect faces in a given image and 
        crops the image to the first detected face.

        Parameters
        ----------
        img : the image in which a face should be detected 

        Returns
        -------
        cropped_image : the image cropped around the first detected face 
        None : if no faces are detected 
        
        """
        results = detector.detect_faces(img)
        if results:
            x1, y1, width, height = results[0]['box']
            x1, y1 = abs(x1), abs(y1)
            x2, y2 = x1 + width, y1 + height 
            cropped_image = img[y1:y2, x1:x2]
        else: 
            print('no faces are detected')
            return None
        return cropped_image
    
    
    
    def initUI(self):
        """ Initilizes the UI components of the application. """
        
        #central widget and layout 
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        layout = QVBoxLayout()

        # setup label for live feed display 
        self.label = QLabel(self)
        layout.addWidget(self.label)

        # setup the buttons and connect their click events:
            
        #Verification button 
        self.verify_button = QPushButton('Verify', self)
        self.verify_button.clicked.connect(self.verify_face)
        layout.addWidget(self.verify_button)
        
        #Add ID button 
        self.add_id_button = QPushButton('Add Person_ID',self)
        self.add_id_button.clicked.connect(self.add_ID)
        layout.addWidget(self.add_id_button)
        
        
        # add text box 
        self.output = QTextEdit(self)  
        self.output.setReadOnly(True)
        layout.addWidget(self.output)
        sys.stdout = OutputWriter(self.output)
        
        # Exit Button 
        self.exitButton = QPushButton("Exit", self) 
        self.exitButton.clicked.connect(self.exit_app)  
        layout.addWidget(self.exitButton)

        # Set the layout for the central widget
        self.central_widget.setLayout(layout)

    
        # setup , start the time for live feed updates
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(30)
        
        #initialize webcam capture 
        self.cap = cv2.VideoCapture(0)

    def update_frame(self):
        
        """
        this function captures a frame from the webcam, converts it to the correct colors
        and updates the GUI label with the captured image. 
        """
        ret, frame = self.cap.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
        self.label.setPixmap(QPixmap.fromImage(image))

    
    
    def init_connx(self):
        """
        Initialize and create a socket connection 
        """
        global client_socket 
        try:
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.connect((SERVER_HOST, SERVER_PORT))
        except Exception as e:
            print(f'Error establishing connection: {e}')
            

    def save_ID(self):
        
        """
        a function that allows the user to enter an ID and select a location
        to save the image file. 
        The file should has a '.jpg' extension and notifies the user if no ID is entered. 
        the function returns the full path of the saved image file 
        """
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        # Set the default file name to an empty string so the user has to enter something
        default_file_name = ""
        # Prompt the user to enter an ID and choose a location to save the image
        file_name, _ = QFileDialog.getSaveFileName(self, "Enter ID and Save Image",
                                                   default_file_name,
                                                   "Images (*.jpg);;All Files (*)",
                                                options=options)
        
        # Check if a filename was entered
        if file_name:
            # Check if user entered a name without the extension and append '.jpg' if necessary
            if not file_name.lower().endswith('.jpg'):
                file_name += '.jpg'
            return file_name
        else:
            # Show a message box if the user didn't enter a name
            QMessageBox.information(self, "No ID Entered", "You must enter an ID to save the image.")
            return None


    def add_ID(self):
        ret, frame = self.cap.read() 
        if ret: 
            detected_face = self.face_detection(frame)
            if detected_face is not None:
                id_name_path = self.save_ID()
                if id_name_path:
                    cv2.imwrite(id_name_path, frame)
                    self.send_ID(id_name_path)
                else: 
                    self.output.setText('NOT A VALID PATH')
            else: 
                self.output.setText('NO FACE Detected')
                
                
                
    def verify_face(self):
        """
        a function to capture an image using the webcam, 
        detect a face in the captured image, 
        save the face and send it for verification 
        """
            
        print("Verifying...")
        #Read the frame from the live feed (webcam)
        ret, frame = self.cap.read()
        
        if ret:
            #detect the face in the captured frame 
            detected_face = self.face_detection(frame)
            if detected_face is not None:
                cv2.imwrite(pic_path, detected_face)
                self.output.setText('detcted face is captured and saved. Proceeding to send for verification...')
                #send the detected and croped frame for verification 
                self.send2verification(pic_path)
            else:
                self.output.setText('No face detected')
        
        
    
    def send2verification(self,pic_path): 
        """
        this function sends the captured face to the socket server for verification 
        
        Parameters:
        pic_path : The path of the image to be sent for verification.
        """
        global client_socket
        try:
            # connect to the server 
            if client_socket is None: 
               self.init_connx()
               
            BUFFER_SIZE = 4096
            #informe the server about the action (verification)
            client_socket.sendall(b'VERIFY\n')
            file_size = os.path.getsize(pic_path)
            client_socket.sendall(struct.pack('<Q', file_size))
            
            #send the file in chunks 
            with open(pic_path, "rb") as file:
                while True:
                    bytes_read = file.read(BUFFER_SIZE)
                    if not bytes_read:
                        break
                    client_socket.sendall(bytes_read)
            
            self.output.append('Image sent successfully.')
            #receive response from the server 
            data = client_socket.recv(1024)
            person_ID = data.decode('utf-8')
            self.output.append(person_ID)
  
        except Exception as e:
            self.output.setText(f"An error occurred in Send2Verification : {e}")
            
            
        
    def send_ID(self,id_path):
        """
        
        send a new ID and the associated image to the server 
        for database registration. 
        
        Parameters:
            id_path: The path of the image associated with the new ID.
        """
        global client_socket
        try:
           #connect to server 
            if client_socket is None: 
              self.init_connx()
            
            BUFFER_SIZE = 4096
            
            #inform the server about the action (add_ID)
            client_socket.sendall(b'ADD_ID\n')
            
            #send the name of the ID_file 
            client_socket.sendall(struct.pack('<I', len(os.path.basename(id_path))))
            client_socket.sendall(os.path.basename(id_path).encode('utf-8'))
            
            file_size = os.path.getsize(pic_path)
            client_socket.sendall(struct.pack('<Q', file_size))
            
            #send the file in chunks 
            with open(pic_path, "rb") as file:
                while True:
                    bytes_read = file.read(BUFFER_SIZE)
                    if not bytes_read:
                        break
                    client_socket.sendall(bytes_read)
            
            self.output.append('Image sent successfully.')
        except Exception as e:
            self.output.setText(f"An error occurred in SendID : {e}")
      
            
    def cleanup(self):
        print("Releasing resources")
        self.cap.release()   
        
      
    def exit_app(self):  
        """ 
        function to exit the Face_ID app and close the socket connection 
        """
        global client_socket
        try:
            if client_socket is None: 
              self.init_connx()
             #inform the server about the action (Exit / Quit)
            client_socket.sendall(b'QUIT\n')
            client_socket.close()
            client_socket = None
        except Exception as e:
            self.output.setText(f"An error occurred: {e}")
        finally:
            self.cap.release()
        self.close()


    def closeEvent(self, event):
        self.timer.stop()  
        if self.cap.isOpened():
            self.cap.release()  
        event.accept()
    
    
    
if __name__ == "__main__":
    FaceID_APP = QApplication(sys.argv)
    window = FaceId()
    window.show()
    sys.exit(FaceID_APP.exec_())