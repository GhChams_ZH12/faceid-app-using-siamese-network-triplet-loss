
import os 
import struct 
import socket 
import cv2
import numpy as np
from tflite_runtime.interpreter import Interpreter



# set the IP and PORT for the server to listen on
CNNX_IP = '0.0.0.0' 
CNNX_PORT = 9999
# buffer size for socket communication
BUFFER_SIZE = 4096


def handle_op(client_socket):
    
    """
    handles different actions received from the client/the user: 
        VERIFY : verification -> return the personID , calls verify_ID 
        ADD_ID : add a person'ID to the dataset folder , calls add_ID
        QUIT : close the socket connexion and exit the FaceID-App 
        
    """
    try: 
        while True:
            operation = b''
            while True: 
                chunk = client_socket.recv(1)
                if chunk == b'\n':
                    break
                operation += chunk 

            print('Operation : ' , operation)
            if operation == b'VERIFY':
                verify_ID(client_socket)
            elif operation == b'ADD_ID':
                add_ID(client_socket)
            elif operation == b'QUIT':
                print("Client requested to close the connection.")
                client_socket.close()
                return False
            else:
                print("Invalid operation request received.")
    except Exception as e:
        print(f"An error occurred: {e}")
    finally:
        client_socket.close()
    return True 




def start_server():      
    """
    starts a socket server that listen for incoming connections 
    handels operations requested by clients. 
    """
   
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #create a TCP/IP socket 
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #set socket options 
    server_socket.bind((CNNX_IP, CNNX_PORT)) #bind the socket 
    
    #listen for incoming connections 
    server_socket.listen(1) #only one connection is allowed 
    print(f'Server listening on {CNNX_IP}:{CNNX_PORT} ... ')
    
    while True: 
        client_socket, address = server_socket.accept() #accept a new connection
        print(f'Connection from {address} has been established.') 
        run_server = handle_op(client_socket) #handel operations 
        if not run_server: #break the loop to stop the server 
            break 
    #close the server socket 
    server_socket.close()
    print ('Shutdown Server ') 


def verify_ID(client_socket):
    """
    function to receive the face image from the socket client 
    process the image to get the personID
    and send a message back to the client with the person's ID
    
    """
    #face to verify 
    img_path = 'path/to/the/recieved/anchor_img' 
    # Ddataste folder 
    data_folder = 'path/to/dataset/folder'
    #tflite model Path
    tflite_model_path = 'path/to/tf/lite/model'
    
    #get the size of the incoming image 
    img_size_data = client_socket.recv(8)
    img_size = struct.unpack('<Q', img_size_data)[0]
    received_size = 0
    
    #receive and write the image data to a file 
    with open(img_path, "wb") as img:
        while received_size < img_size:
            bytes_read = client_socket.recv(min(BUFFER_SIZE, img_size - received_size))
            if not  bytes_read:
                print("End Of File transmission") 
                break
            img.write(bytes_read)
            received_size += len(bytes_read)

    print('IMAGE received Successfully') 
    

    # Predict the Person_ID and the computed distance using the TFLite model . 
    person_name, prediction_value = get_person_ID(img_path, data_folder, tflite_model_path)

    message = f"Hallo {person_name} "
    print(message)
    #send the message back to the client 
    client_socket.sendall(message.encode('utf-8')) 

def get_person_ID(anchor_img_path, data_folder,tflite_model_path):
    """ 
    identifies the person's ID in the anchor (received img) by comparing 
    it against a dataset of imgs. 
    
    Parameters
    ----------
    anchor_img_path : path to the received img (captured via the webcam on the client)
    data_folder : path to dataset folder 
    tflite_model_path : path to the TFLite Model file 
    
    Returns
    -------
    Person_ID : the ID of the identified person and the computed distance value
    """
    
    # load and preprocess the cpatured/received img 
    anchor_img = cv2.imread(anchor_img_path)
    anchor_img = preprocess(anchor_img)

    # initialize the variables 
    smallest_prediction = float('inf')
    Person_ID = None

    # compare the anchor with each imge in the dataset 
    for filename in os.listdir(data_folder):
        if filename.endswith('.jpg'): 
            pos_img_path = os.path.join(data_folder, filename)
            pos_img = cv2.imread(pos_img_path)
            pos_img = preprocess(pos_img)

            # get the prediction with TFLite-Modell
            prediction = predict(tflite_model_path, anchor_img, pos_img )
            
            #  Update the smallest prediction and person ID if current prediction is smaller
            if prediction < smallest_prediction:
                smallest_prediction = prediction
                Person_ID = os.path.splitext(os.path.basename(filename))[0]
                
        # Set Person_ID to 'Unknown-ID' if no match is found within the threshold
        if smallest_prediction > 10 : # adjust the threshold 
            Person_ID = 'Unkown-ID'
    return Person_ID, smallest_prediction



def preprocess(img):
    img = cv2.resize(img, (224, 224))  # Resize
    img = img.astype(np.float32)
    img = np.expand_dims(img, axis=0)  # Expand dimensions
    return img


def predict(model_path,captured,verification):
    """
    Function to predict with the TFLite Model 

    Parameters
    ----------
    model_path:   path to the TFLite Model file 
    captured:     path to the anchor img (captured via the webcam on the client)
    verification: path to the positive img 


    Returns
    -------
    output_data : distance between the anchor and positiv img 

    """
    ndummy_img = np.zeros_like(verification) # dummy negativ img 
    # Load TFLite model and allocate tensors
    
    interpreter = Interpreter(model_path=model_path)
    interpreter.allocate_tensors()

    # Get input and output tensors
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    # Make predictions
    interpreter.set_tensor(input_details[0]['index'], captured)
    interpreter.set_tensor(input_details[1]['index'], verification)
    interpreter.set_tensor(input_details[2]['index'], ndummy_img)
    
    interpreter.invoke()
    output_data = interpreter.get_tensor(output_details[1]['index'])

    return output_data



def add_ID(client_socket):
    """ 
    get an img and its ID from the client 
    saves the img to a specified directory 
    """
    try:
        #receive the length of the imageID
        name_len_data = client_socket.recv(4)
        name_len = struct.unpack('<I', name_len_data)[0]
        #receive the img ID 
        img_id = client_socket.recv(name_len).decode('utf-8')
        
        #define the path where to save the img 
        img_id_path = f'face_id/pos_data/{img_id}' 
        
        #receive the size of the incoming img 
        img_size_data = client_socket.recv(8)
        img_size = struct.unpack('<Q', img_size_data)[0]
        received_size = 0
        
        #receive and write the img to a file 
        with open(img_id_path, "wb") as img:
            while received_size < img_size:
                bytes_read = client_socket.recv(min(BUFFER_SIZE, img_size - received_size))
                if not  bytes_read:
                    print("End Of File transmission") 
                    break
                img.write(bytes_read)
                received_size += len(bytes_read)

        print(f'IMAGE {img_id_path} received Successfully') 
    except Exception as e:
        print(f'an Error occured: {e}')
        



    
    
    
#start the server     
start_server() 
