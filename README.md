# FaceID App using Siamese Network Triplet Loss 


## Description 
This FaceID Application uses a Siamese Network with triplet loss method based on pre-trained ResNet50 convolutional neural network (CNN). The model is trained on the VggFace2 HQ cropped dataset using TensorFlow. The model was optimized and converted to TensorFlow Lite (TFLite) for efficient deployment and compatibility on the processing system (PS) part of a Zynq UltraScale+ Plattform, which runs on Debian 12.  
The standalone Siamese Network Model computes the embedding distance between face images to identify individuals.  
A key feature of this application is the socket connection integration for efficient communication between client and server (Zynq UltraScale+). The FaceID App facilites the identification of individuals and enables the addition of new IDs to the Verification Database. The Application uses the computed distances to predict the correct identity of a person. This approach offers a robust solution for real-time facial recognition and identity verification tasks 

## Dataset Structure 
The verification database folder is organized in a simple and effective structure: 

Database/<br />
 │<br />
 ├── bob.jpg<br />
 ├── lisa.jpg<br />
 └── ...<br />

Each image file is named after the person it represents. 

## Model Training and Conversion  
The Siamese Network was trained using TensorFlow on the VggFace2 HQ cropped dataset (https://www.kaggle.com/datasets/zenbot99/vggface2-hq-cropped). The main goal is to minimize the distance between similar faces and maximize it between different faces. 

To execute the model on the Zynq UltraScale+, the model was converted to the TFLite format. 

## Implementation 
The main component of the Application is the TFLite model. This model was implemented on the zynq UltraScale+ platform's processing system, to ensure a real-time facial identification system. 
After testing the model on the zynq platform, it was integrated into a PyQt-based application, establishing a socket-based client-server architecture. The zynq acts as the server, processing distance computation for identification as well as adding new IDs from client-sent images. This structure optimizes the application for continuous real-time facial recognition and dataset felxibility. 

